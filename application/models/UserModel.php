<?php
class UserModel extends CI_Model
{
    public function getPosts($limit,$offset) //offset starting row number
    {
        $this->db->limit($limit, $offset);
        $query = $this->db->get('user_details');
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }
    public function num_rows()
    {
        $query = $this->db->get('user_details');
        if($query->num_rows()>0)
        {
            return $query->num_rows();
        }
    }


    public function addPost($data)
   {
       return $this->db->insert('user_details',$data);
   }
   public function getSinglePosts($id)
   {
       $query = $this->db->get_where('user_details', array('id' => $id));
       if($query->num_rows()>0)
       {
           return $query->row();//return single row from the db
       }

   }
    public function updatePost($data,$id)
    {
        return $this->db->where('id',$id)
            ->update('user_details',$data);
    }
    public function deletePost($id)
    {
        return $this->db->delete('user_details',['id'=>$id]); //return 1 if the data is deleted from the table
    }
}


?>