
<?php

class Users extends CI_Controller
{
    public function __construct()
    {
        //echo "milse";
        parent::__construct();

        $this->load->model('userModel');
    }
    public function index() //return all posts
    {
    /*    $config['base_url']='index';
        $config['total_rows'] =$this->userModel->num_rows();
        $config['per_page'] = 3;*/
        $config=[
            'base_url'   =>base_url('index.php/users/index'),
            'total_rows' =>$this->userModel->num_rows(),
            'per_page'   =>3,
            'full_tag_open' 	=> '<div class="pagging text-center"><nav><ul class="pagination">',
        'full_tag_close'	=> '</ul></nav></div>',
        'num_tag_open' 	=> '<li class="page-item"><span class="page-link">',
        'num_tag_close' 	=> '</span></li>',
        'cur_tag_open' 	=> '<li class="page-item active"><span class="page-link">',
        'cur_tag_close' 	=> '<span class="sr-only">(current)</span></span></li>',
        'next_tag_open' 	=> '<li class="page-item"><span class="page-link">',
        'next_tagl_close' 	=> '<span aria-hidden="true">&raquo;</span></span></li>',
        'prev_tag_open' 	=> '<li class="page-item"><span class="page-link">',
        'prev_tagl_close' 	=> '</span></li>',
        'first_tag_open' 	=> '<li class="page-item"><span class="page-link">',
        'first_tagl_close' => '</span></li>',
        'last_tag_open'	=> '<li class="page-item"><span class="page-link">',
        'last_tagl_close' 	=> '</span></li>'

        ];
        $this->pagination->initialize($config);
        $posts=$this->userModel->getPosts($config['per_page'],$this->uri->segment(3, 0));
        $this->load->view('users/userlist',['posts'=>$posts]);
    }
    public function is_password_strong($password)
    {
       // echo $password;
//regular expression
        if (preg_match('#[0-9]#', $password) && preg_match('#[a-zA-Z]#', $password)) {
            return TRUE;
        }
        return FALSE;
    }
    //when add post is clicked
    public function add() //create form called
    {
        //echo "dhukse";
        $this->form_validation->set_rules('full_name', 'Full Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]|callback_is_password_strong');
        $this->form_validation->set_rules('confirm_pass', 'Confirm Password', 'required|matches[password]');
        $this->form_validation->set_message('is_password_strong', 'Password is not strong');

//when submit button is pressed
        if($this->form_validation->run())
        {
            $data = $this->input->post();

            unset($data['submit']);
            $this->userModel->addPost($data);
            return redirect('users/index');
        }
        else
        {

            $this->load->view('users/create');
        }
    }
    public function edit($id)
    {

        $this->form_validation->set_rules('full_name', 'Full name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        if ($this->form_validation->run())
        {
            $data = $this->input->post();
           // print_r($data);
            unset($data['submit']);
            // print_r($data);
            $this->load->model('userModel');
            $this->userModel->updatePost($data,$id);
            return redirect('users/index');
        }
        else
        {

         //   $this->load->model('userModel');
            $post = $this->userModel->getSinglePosts($id);
            $this->load->view('users/edit',['post'=>$post]);
        }
    }

    public function delete($id)
    {

        $this->load->model('userModel');
        $this->userModel->deletePost($id);
        return redirect('users/index');

    }
}
