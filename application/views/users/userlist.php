<?php
include_once('header.php');
?>

    <div class="container">
        <h3>View All Posts</h3>

        <?php
        echo anchor('users/add', 'Add Post', array('class' => 'btn btn-primary'));
        // Prints: <a href="http://example.com/index.php/news/local/123" title="The best news!">My News</a>
        ?>
        <?php
        $atts = array(
            'width'       => 800,
            'height'      => 600,
            'scrollbars'  => 'yes',
            'status'      => 'yes',
            'resizable'   => 'yes',
            'screenx'     => 0,
            'screeny'     => 0,
            'window_name' => '_blank'
        );

        ?>

        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Full Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Address</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($posts)):?>
                <?php foreach ($posts as $post):?>
                    <tr class="table-active">
                        <td><?php echo $post->full_name ?></td>
                        <td><?php echo $post->email ?></td>
                        <td><?php echo $post->phone?></td>
                        <td><?php echo $post->address?></td>
                        <td><?php echo $post->status?></td>
                        <td>
                            <?php  // echo anchor("welcome/show/{$post->id}", 'View', array('class' => 'badge badge-primary')); ?>
                            <?php   echo anchor("users/edit/{$post->id}", 'Edit', array('class' => 'badge badge-success'));?>
                            <?php   echo anchor("users/delete/{$post->id}", 'Delete', array('class' => 'badge badge-danger', 'onclick'=>"return confirm('Are you sure you want to delete?')"));?>
                        </td>
                    </tr>
                <?php endforeach;?>
            <?php else: ?>
                <tr>
                    <td>
                        No Records Found!
                    </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <div class="row">

                <div class="col">
                    <div class="float-right"><?php

                        echo $this->pagination->create_links();
                        ?>
                    </div>
                </div>
        </div>

        <!--
        <nav aria-label="...">
        -->



    </div>




<?php
include_once('footer.php');
?>
