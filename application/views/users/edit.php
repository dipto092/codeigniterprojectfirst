<?php
include_once('header.php');
?>
<div class="container">

    <?php echo form_open("users/edit/{$post->id}",['class'=>'form-horizontal'])?>
    <?php
    //print_r($post);
    ?>
    <fieldset>
        <legend>Add a new user</legend>

        <div class="form-group"> <!--optimum spacing-->
            <label for="full name" class="col-md-2 control-label">Full Name</label>
            <div class="col-md-5">
                <?php echo form_input(['name'=>'full_name','placeholder'=>'Enter Full name','class'=>'form-control','value'=>set_value('full_name',$post->full_name)]); ?>
            </div>
            <div class="col-md-5">
                <?php echo form_error('full_name', '<div class="text-danger">', '</div>'); ?>
            </div>

        </div>
        <div class="form-group">
            <label for="email" class="col-md-2 control-label">Email</label>
            <div class="col-md-5">

                <?php echo form_input(['name'=>'email','placeholder'=>'Enter Email','class'=>'form-control','value'=>set_value('email',$post->email)]); ?>
            </div>
            <div class="col-md-5">
                <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="phone" class="col-md-2 control-label">Phone</label>
            <div class="col-md-5">

                <?php echo form_input(['name'=>'phone','placeholder'=>'Enter Phone number','class'=>'form-control','value'=>set_value('phone',$post->phone)]); ?>
            </div>
        </div>

        <div class="form-group">
            <label for="address" class="col-md-2 control-label">Address</label>
            <div class="col-md-5">

                <?php echo form_input(['name'=>'address','placeholder'=>'Enter Address','class'=>'form-control','value'=>set_value('address',$post->address)]); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="status" class="col-md-2 control-label">Enter Status</label>
            <div class="col-md-5">

                <?php echo form_input(['name'=>'status','placeholder'=>'Enter Status','class'=>'form-control','value'=>set_value('status',$post->status)]); ?>
            </div>
        </div>


        <div class="form-group">
            <div class="col-md-10 col-md-offset-2">
                <?php echo form_submit(['name'=>'submit','value'=>'Submit','class'=>'btn btn-primary']); ?>
                <?php //echo anchor('welcome/index','Back',['class'=>'btn btn-default']); ?>
            </div>
        </div>

    </fieldset>
    <?php
    echo form_close();
    ?>

</div>












<?php
include_once('footer.php');
?>


